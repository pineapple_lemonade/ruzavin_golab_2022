package internal

import (
	"errors"
	"fmt"
	"strings"
)

const PhonePrefix = "+7"

type User struct {
	FirstName string
	LastName  string
	Age       int
	Phone     string
}

func (user User) ToString() {
	fmt.Printf("{Age = %v, FirstName = %v, LastName = %v, Phone = %v}",
		user.Age, user.FirstName, user.LastName, user.Phone)
}

func (user *User) ChangePhone(phone string) {
	if !strings.HasPrefix(phone, PhonePrefix) {
		panic(errors.New("phone number must starts with +7"))
	}

	user.Phone = phone
}
