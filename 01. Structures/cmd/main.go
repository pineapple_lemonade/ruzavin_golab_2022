package main

import "01._Structures/internal"

func main() {
	user := internal.User{
		FirstName: "gosha",
		LastName:  "ruzavin",
		Age:       10,
		Phone:     "12312",
	}

	user.ToString()
	user.ChangePhone("+79961073194")
	user.ToString()
}
